﻿using BudGestion.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BudGestion.DAL.Repositories
{
    public class BudgestionContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<MonthlyBudget> MonthlyBudgets { get; set; }
        public DbSet<BudgetCategory> BudgetCategories { get; set; }
        public DbSet<BudgetLine> BudgetLines { get; set; }
        //public DbSet<Enveloppe> Enveloppes { get; set; }

        public BudgestionContext(DbContextOptions options) : base(options)
        { }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<BudgetCategory>().HasOne(b => b.MonthlyBudget).WithMany(c => c.BudgetCategories).HasForeignKey(i => i.MonthlyBudgetId);
            builder.Entity<BudgetLine>().HasOne(c => c.BudgetCategory).WithMany(l => l.BudgetLines).HasForeignKey(i => i.BudgetCategoryId);
        }
    }
}
