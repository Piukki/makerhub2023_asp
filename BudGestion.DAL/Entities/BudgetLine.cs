﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BudGestion.DAL.Entities
{
    public class BudgetLine
    {
        public int Id { get; set; }
        public string? BudgetName { get; set; }
        public float PlannedAmmount { get; set; }

        public BudgetCategory BudgetCategory { get; set; }
        public int BudgetCategoryId { get; set; }
    }
}
