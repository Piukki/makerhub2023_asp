﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BudGestion.DAL.Entities
{
    public class MonthlyBudget
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastUpdated { get; set; }
        public DateTime StartAt { get; set; }
        public DateTime EndAt { get; set; }
        public float LeftToSpend { get; set; }

        public User User { get; set; }
        public int UserId { get; set; }

        public virtual ICollection<BudgetCategory> BudgetCategories { get; set; }

    }
}
