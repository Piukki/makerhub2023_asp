﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BudGestion.DAL.Entities
{
    public class BudgetCategory
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public float TotalCategory { get; set; }

        public MonthlyBudget MonthlyBudget { get; set; }
        public int MonthlyBudgetId { get; set; }

        public virtual ICollection<BudgetLine>? BudgetLines { get; set; }
    }
}
