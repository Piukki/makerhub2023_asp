﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BudGestion.DAL.Entities
{
    public class User
    {   
        public int Id { get; set; }
        public string UserName { get; set; }
        public string? Token { get; set; }
        public string Email { get; set; }
        public byte[]? PasswordHash { get; set; }
        public byte[]? PasswordSalt { get; set; }

        public ICollection<MonthlyBudget>? MonthlyBudgets { get; set; }
    }
}
