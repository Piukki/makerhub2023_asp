﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BudGestion.DAL.Entities
{
    public class Enveloppe
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public float Amount { get; set; }
        public DateTime StartAt { get; set; }
        public DateTime EndAt { get; set; }

    }
}
