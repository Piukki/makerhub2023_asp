﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BudGestion.DAL.Migrations
{
    /// <inheritdoc />
    public partial class AddTotal : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<float>(
                name: "TotalCategory",
                table: "BudgetCategories",
                type: "real",
                nullable: false,
                defaultValue: 0f);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TotalCategory",
                table: "BudgetCategories");
        }
    }
}
