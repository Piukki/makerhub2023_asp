﻿using BudGestion.BLL.DTO;
using BudGestion.BLL.Mappers;
using BudGestion.DAL.Entities;
using BudGestion.DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BudGestion.BLL.Services
{
    public class MonthlyBudgetService
    {
        private readonly BudgestionContext _budgestionContext;

        public MonthlyBudgetService(BudgestionContext budgestionContext)
        {
            _budgestionContext = budgestionContext;
        }

        public void AddMonthlyBudget(AddMonthlyBudgetDTO dto)
        {
            MonthlyBudget newMonthlyBudget = MonthlyBudgetMapper.ToDbMonthlyBudgetDTO(dto);
            _budgestionContext.Add(newMonthlyBudget);
            _budgestionContext.SaveChanges();
            
        }

        public IEnumerable<MonthlyBudgetDetailDTO> GetMonthlyBudget()
        {
            return _budgestionContext.MonthlyBudgets
                .Include(m => m.BudgetCategories)
                .ThenInclude(c => c.BudgetLines)
                .Select(MonthlyBudgetMapper.GetMonthlyBudgetDetailsDTO);
        }

        public MonthlyBudgetDetailDTO GetMonthlyBudgetById(int id)
        {
            return _budgestionContext.MonthlyBudgets
                .Include(m => m.BudgetCategories)
                .ThenInclude(c => c.BudgetLines)
                .Where(m => m.Id == id)
                .Select(MonthlyBudgetMapper.GetMonthlyBudgetDetailsDTO)
                .FirstOrDefault();
        }

        public string Delete(int id)
        {
            MonthlyBudget? budget = _budgestionContext.MonthlyBudgets.FirstOrDefault(b => b.Id == id);
            if (budget == null) return "Pas de budget trouvé";
            _budgestionContext.Remove(budget);
            _budgestionContext.SaveChanges();
            return "Budget supprimé avec succès";
        }

        public void Update(int id, UpdateMonthlyBudgetDTO updateBudget)
        {
            MonthlyBudget? budget = _budgestionContext.MonthlyBudgets
                .Include(m => m.BudgetCategories)
                .ThenInclude(c => c.BudgetLines)
                .FirstOrDefault(u => u.Id == id);

            if (budget == null)
            {
                throw new KeyNotFoundException("Impossible de mettre à jour le budget");
            }
            budget = updateBudget.UpdatemonthlyBudgetToDbDTO(budget);

            _budgestionContext.MonthlyBudgets.Update(budget);
            _budgestionContext.SaveChanges();
        }

    }
}
