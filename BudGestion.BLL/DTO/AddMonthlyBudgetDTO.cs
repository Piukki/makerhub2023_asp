﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BudGestion.BLL.DTO
{
    public class AddMonthlyBudgetDTO
    {
        public DateTime StartAt { get; set; }
        public DateTime EndAt { get; set; }
        public float LeftToSpend { get; set; }
        public IEnumerable<BudgetCategoryDetailDTO> BudgetCategories { get; set; }
    }
}
