﻿using BudGestion.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BudGestion.BLL.DTO
{
    public class BudgetCategoryDetailDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public float TotalAmmount { get; set; }
        public float AmmountLeft { get; set; }

        public IEnumerable<BudgetLineDetailDTO>? BudgetLines { get; set; }
    }
}
