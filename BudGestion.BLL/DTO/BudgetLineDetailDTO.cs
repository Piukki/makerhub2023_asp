﻿using BudGestion.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BudGestion.BLL.DTO
{
    public class BudgetLineDetailDTO
    {
        public int Id { get; set; }
        public string? BudgetName { get; set; }
        public float PlannedAmmount { get; set; }
        
    }
}
