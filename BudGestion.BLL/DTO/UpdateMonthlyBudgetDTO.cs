﻿using BudGestion.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BudGestion.BLL.DTO
{
    public class UpdateMonthlyBudgetDTO
    {
        public int Id{ get; set; }
        public DateTime LastUpdated { get; set; }
        public DateTime StartAt { get; set; }
        public DateTime EndAt { get; set; }
        public IEnumerable<BudgetCategoryDetailDTO> BudgetCategories { get; set; }

    }
}
