﻿using BudGestion.BLL.DTO;
using BudGestion.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BudGestion.BLL.Mappers
{
    public static class BudgetCategoryMapper
    {
        public static BudgetCategoryDetailDTO CategoryDetailDTO(this BudgetCategory bc)
        {
            return new BudgetCategoryDetailDTO
            {
                Id = bc.Id,
                Name = bc.Name,
                BudgetLines = bc.BudgetLines.Select(l => l.ToLineDetailDTO()),
                TotalAmmount = bc.BudgetLines.Sum(t => t.PlannedAmmount)
            };
        }
    }
}
