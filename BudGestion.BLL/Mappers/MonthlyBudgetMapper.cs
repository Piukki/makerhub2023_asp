﻿using BudGestion.BLL.DTO;
using BudGestion.DAL.Entities;
using BudGestion.BLL.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.NetworkInformation;

namespace BudGestion.BLL.Mappers
{
    public static class MonthlyBudgetMapper
    {
        public static MonthlyBudget ToDbMonthlyBudgetDTO(this AddMonthlyBudgetDTO dto)
        {
            return new MonthlyBudget()
            {
                UserId = 6,
                StartAt = dto.StartAt,
                EndAt = dto.EndAt,
                CreationDate = DateTime.Now,
                LastUpdated = DateTime.Now,
                LeftToSpend = dto.LeftToSpend,
                BudgetCategories = dto.BudgetCategories.Select(c => new BudgetCategory
                {
                    Name = c.Name,
                    BudgetLines = c.BudgetLines.Select(l => new BudgetLine
                    {
                        BudgetName = l.BudgetName,
                        PlannedAmmount = l.PlannedAmmount
                    }).ToList()
                }).ToList()
            };
        }

        public static MonthlyBudgetDetailDTO GetMonthlyBudgetDetailsDTO(this MonthlyBudget budget)
        {
            return new MonthlyBudgetDetailDTO()
            {
                Id = budget.Id,
                CreationDate = budget.CreationDate,
                LastUpdated = budget.LastUpdated,
                StartAt = budget.StartAt,
                EndAt = budget.EndAt,
                LeftToSpend = budget.LeftToSpend,
                BudgetCategories = budget.BudgetCategories.Select(c => c.CategoryDetailDTO()),
                User = budget.User,  
            };
        }

        public static MonthlyBudget UpdatemonthlyBudgetToDbDTO(this UpdateMonthlyBudgetDTO budget, MonthlyBudget toUpdate)
        {
            toUpdate.LastUpdated = DateTime.Now;
            toUpdate.StartAt = budget.StartAt;
            toUpdate.EndAt = budget.EndAt;
            toUpdate.BudgetCategories = budget.BudgetCategories.Select(c => new BudgetCategory
            {
                Name = c.Name,
                BudgetLines = c.BudgetLines.Select(l => new BudgetLine
                {
                    BudgetName = l.BudgetName,
                    PlannedAmmount = l.PlannedAmmount
                }).ToList()
            }).ToList();

            return toUpdate;
        }
    }
}
