﻿using BudGestion.BLL.DTO;
using BudGestion.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BudGestion.BLL.Mappers
{
    public static class BudgetLineMapper
    {
        public static BudgetLineDetailDTO ToLineDetailDTO(this BudgetLine bl)
        {
            return new BudgetLineDetailDTO()
            {
                Id = bl.Id,
                BudgetName = bl.BudgetName,
                PlannedAmmount = bl.PlannedAmmount
            };
        }
    }
}
