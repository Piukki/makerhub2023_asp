﻿using BudGestion.BLL.DTO;
using BudGestion.BLL.Mappers;
using BudGestion.BLL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BudGestion.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MonthlyBudgetController : ControllerBase
    {

        private readonly MonthlyBudgetService _monthlyBudgetService;

        public MonthlyBudgetController(MonthlyBudgetService monthlyBudgetService)
        {
            _monthlyBudgetService = monthlyBudgetService;
        }

        [HttpPost]
        public IActionResult Add(AddMonthlyBudgetDTO dto)
        {
            _monthlyBudgetService.AddMonthlyBudget(dto);
            return Ok();
        }

        [HttpGet]
        public IActionResult GetDetails()
        {

            return Ok(_monthlyBudgetService.GetMonthlyBudget());
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                return Ok(_monthlyBudgetService.GetMonthlyBudgetById(id));
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
            catch (Exception)
            {

                throw;
            }

            
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, UpdateMonthlyBudgetDTO dto)
        {
            _monthlyBudgetService.Update(id, dto);
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            Console.WriteLine("ok");
            try
            {
                _monthlyBudgetService.Delete(id);
                return Ok();
            }
            catch (KeyNotFoundException)
            {

                return NotFound();
            }
            catch (Exception)
            {
                throw;
            }
            
        }
    }
}
